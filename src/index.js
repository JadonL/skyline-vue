import Button from './components/Button.vue';
import ButtonRound from './components/ButtonRound.vue';
import Card from './components/Card/Card.vue';
import CardLong from './components/Card/Long.vue';
import Drawer from './components/Drawer.vue';
import Tag from './components/Tag.vue';

export default {
  Button,
  ButtonRound,
  Card,
  CardLong,
  Drawer,
  Tag,
};
