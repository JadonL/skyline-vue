/**
 * @param imgSrc {string} Source location for image.
 * @param accuracy {number} Size of image canvas used in determining dominate color. Default = 200. (Use -1 for full size)
 * @param blockSize {number} Check every x number of pixels in the image. Default = 5
 * @param topPlaces {number} Top dominate colors in the image. Default = 3
 */
export async function getDominateColors(
  imgSrc,
  {accuracy, blockSize, topPlaces, ignoreColors} = {
    accuracy: 200,
    ignoreColors: [],
  },
) {
  return new Promise((resolve, reject) => {
    const canvas = document.createElement('canvas');
    canvas.setAttribute('height', accuracy);
    canvas.setAttribute('width', accuracy);

    const canvasContext = canvas.getContext('2d');

    const image = new Image(accuracy, accuracy);

    image.addEventListener(
      'load',
      function() {
        if (accuracy === -1) {
          canvas.height = this.naturalHeight;
          canvas.width = this.naturalWidth;
        }

        canvasContext.drawImage(this, 0, 0, canvas.width, canvas.height);

        let imageData;
        try {
          imageData = canvasContext.getImageData(
            0,
            0,
            canvas.width,
            canvas.height,
          );
        } catch (e) {
          console.error('Unable to get image data', e);
        }

        resolve(
          determineDominateColorsFromImageData(
            imageData,
            blockSize,
            topPlaces,
            ignoreColors,
          ),
        );
      },
      false,
    );
    image.src = imgSrc;
  });
}

function determineDominateColorsFromImageData(
  imageData,
  blockSize = 5,
  topPlaces = 3,
  ignoreColors = [],
) {
  const {data} = imageData;
  const imageDataLength = data.length;
  const imageDataMap = new Map();
  let i = -4;

  while ((i += blockSize * 4) < imageDataLength) {
    const pixelKey = `rgb(${data[i]}, ${data[i + 1]}, ${data[i + 2]})`;
    let count = 1;

    if (imageDataMap.has(pixelKey)) {
      count += imageDataMap.get(pixelKey);
    }

    imageDataMap.set(pixelKey, count);
  }

  const mostCommonColors = [...imageDataMap].sort((a, b) => b[1] - a[1]);

  topPlaces =
    topPlaces >= mostCommonColors.length
      ? mostCommonColors.length - 1
      : topPlaces;

  //TODO: Do not count ignored colors in final return
  return mostCommonColors
    .filter((c, i) => i < topPlaces && !ignoreColors.includes(c[0]))
    .map(c => c[0]);
}

function determinAverageColorFromImageData(imageData, blockSize = 5) {
  const {data} = imageData;
  let count = 0;
  let i = -4;
  let rgb = {r: 0, g: 0, b: 0};
  let length = data.length;

  while ((i += blockSize * 4) < length) {
    ++count;
    rgb.r += data[i];
    rgb.g += data[i + 1];
    rgb.b += data[i + 2];
  }

  // ~~ used to floor values
  rgb.r = ~~(rgb.r / count);
  rgb.g = ~~(rgb.g / count);
  rgb.b = ~~(rgb.b / count);

  console.log(rgb);
}
