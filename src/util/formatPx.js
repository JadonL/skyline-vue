export default px => (typeof px === 'string' ? px : `${px}px`);
