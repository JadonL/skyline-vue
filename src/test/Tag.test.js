import {shallow} from 'vue-test-utils';
import Tag from '../components/Tag.vue';

describe('Tag Component', () => {
  const defaultPropsData = {
    name: 'Test Tag',
  };
  const defaultSlots = {};

  const render = (propsData = defaultPropsData, slots = defaultSlots) =>
    shallow(Tag, {
      propsData,
      slots,
    });

  it('should render the Tag component with default props and name: "Test Tag"', () => {
    expect(render().element).toMatchSnapshot();
  });

  it('should render the Tag component with name: "<span>Test Tag</span>"', () => {
    expect(
      render(null, {default: '<span>Test Tag</span>'}).element,
    ).toMatchSnapshot();
  });
});
