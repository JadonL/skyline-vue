import {shallow} from 'vue-test-utils';
import Card from '../../components/Card/Card.vue';

describe('Card Component', () => {
  const defaultPropsData = {
    title: 'Test Card',
  };
  const defaultSlots = {};

  const render = (propsData = defaultPropsData, slots = defaultSlots) =>
    shallow(Card, {
      propsData,
      slots,
    });

  it('should render the Card with default props', () => {
    expect(render().element).toMatchSnapshot();
  });
});
