import {shallow} from 'vue-test-utils';
import CardLong from '../../components/Card/Long.vue';

describe('CardLong Component', () => {
  const defaultPropsData = {
    title: 'Test CardLong',
    description: 'The card long componet is a card and it is long.',
    color: '#123456',
  };
  const defaultSlots = {};

  const render = (propsData = defaultPropsData, slots = defaultSlots) =>
    shallow(CardLong, {
      propsData,
      slots,
    });

  it('should render the CardLong component with default props', () => {
    expect(render().element).toMatchSnapshot();
  });
});
