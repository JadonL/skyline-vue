import {shallow} from 'vue-test-utils';
import Button from '../components/Button.vue';

describe('Button Component', () => {
  let cmp;

  beforeEach(() => {
    cmp = shallow(Button, {
      propsData: {
        fontSize: 18,
      },
      slots: {
        default: '<span>Test Button</span>',
      },
    });
  });

  it('should render the Button component with 18px font', () => {
    expect(cmp.element).toMatchSnapshot();
  });
});
