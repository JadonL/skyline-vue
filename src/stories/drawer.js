import Vue from 'vue';
import {storiesOf} from '@storybook/vue';

import Drawer from '../components/Drawer.vue';
import Button from '../components/Button.vue';

storiesOf('Drawer', module)
  .add('Left Drawer', () => ({
    components: {Drawer, Button},
    template: `
		<drawer>
			<Button>Left Button</Button>
		</drawer>
	`,
  }))
  .add('Right Drawer', () => ({
    components: {Drawer, Button},
    template: '<drawer :left="false"><Button>Right Button</Button></drawer>',
  }));
