import Vue from 'vue';
import {storiesOf} from '@storybook/vue';
import VueInfoAddon from 'storybook-addon-vue-info';

import CardCompact from '../components/Card/Card.vue';
import CardLong from '../components/Card/Long.vue';

storiesOf('Card', module)
  .addDecorator(VueInfoAddon)
  .add('Square', () => ({
    data: () => {
      return {
        title: 'Jinglin Joe',
        backgroundImage: require('../assets/joe.jpg'),
      };
    },
    components: {CardCompact},
    template: `
	<card-compact
	  :title="title"
	  :backgroundImage="backgroundImage">
	  <p :style="{ color: '#fff', textShadow: '1px 1px #333' }">
		Uh oh you friccin moron. You just got Jingled!
	  </p>
	</card-compact>
  `,
  }))
  .add('Long', () => ({
    data: () => {
      return {
        title: 'The Stifle Tower',
        description: 'The 2018 NBA Defensive Player of the Year',
        backgroundImage: require('../assets/rudy.jpg'),
      };
    },
    components: {CardLong},
    template: `
		<card-long :title="'The Stifle Tower'"
			:description="description" 
	    		:backgroundImage="backgroundImage">
		<span>You can put additional content here</span>
		</card-long>
	`,
  }))
  .add('Tall', () => ({
    data: () => {
      return {
        title: 'Donovan "Spida" Mitchell',
        backgroundImage: require('../assets/game_blouses.jpg'),
      };
    },
    components: {CardCompact},
    template: `
		<card-compact :height="800" :width="400" :title="title" :backgroundImage="backgroundImage">
		<p :style="{ color: '#fff', textShadow: '1px 1px #333' }">
	    		Game. Blouses.
	  	</p>
	  </card-compact>
	  `,
  }));
