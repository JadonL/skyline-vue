import Vue from 'vue';
import {storiesOf} from '@storybook/vue';

import Button from '../components/Button.vue';

storiesOf('Button', module).add('Default', () => ({
  template: '<Button>Default Button</Button>',
}));
