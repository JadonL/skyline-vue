import Vue from 'vue';
import {storiesOf} from '@storybook/vue';
import Icon from 'vue-awesome/components/Icon';

import Tag from '../components/Tag.vue';

storiesOf('Tag', module)
  .add('Default', () => '<tag><span>React</span></tag>')
  .add('With Icon', () => ({
    components: {Icon},
    template: `
	  <div style="display: flex; align-items: center;">
		  <tag>
			<icon slot="icon" name="brands/react" :height="14" :width="14" />
			<span>React</span>
		  </tag>
		  <tag :customStyles="{ margin: '0 6px', icon: { alignItems: 'flex-end' } }">
			<icon slot="icon" name="brands/vuejs" :height="14" :width="14" />
			<span>Vue</span>
		  </tag>	  
	  </div>
	`,
  }));
