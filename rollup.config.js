import VuePlugin from 'rollup-plugin-vue';

export default {
  input: 'src/index.js',
  output: {
    dir: 'dist',
    file: 'bundle.js',
    name: 'skyline-vue',
    format: 'umd',
  },
  plugins: [VuePlugin()],
};
