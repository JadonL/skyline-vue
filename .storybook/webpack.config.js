const path = require('path');
const VueLoader = require('vue-loader/lib/plugin');

module.exports = (baseConfig, env, defaultConfig) => {
  defaultConfig.plugins.push(new VueLoader());
  return defaultConfig;
};
