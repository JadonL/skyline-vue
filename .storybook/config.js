import Vue from 'vue';
import {configure} from '@storybook/vue';
import 'vue-awesome/icons';

//Components
import Button from '../src/components/Button.vue';
import Tag from '../src/components/Tag.vue';

Vue.component('Button', Button);
Vue.component('tag', Tag);

function loadStories() {
  require('../src/stories');
}

configure(loadStories, module);
